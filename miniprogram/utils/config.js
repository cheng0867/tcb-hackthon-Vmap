class Config {
  constructor() {

  }
}

//模版消息ID
Config.lessonTmplIds = new Array('lQ4P6kuON0riu90bMqdtQoeuAju3bc1l4uvHxa6Ywyw');
//默认位置
Config.defaultLocation = {latitude: 39.908716,longitude: 116.397464};
//默认城市
Config.defaultCity = "110101";
//云环境
Config.cloud = { env: 'xiaotiti-zazc2'};

export { Config };