const cloud = require('wx-server-sdk');
cloud.init();
const db = cloud.database();

exports.main = async (event, context) => {
  try {
    const { name, district, location, templateId, tipId } = event;
    const { OPENID } = cloud.getWXContext();
    // 在云开发数据库中存储用户订阅信息
    const result = await db.collection('messages').add({
      data: {
        touser: OPENID,
        _openid: OPENID,
        name: name,
        district: district,
        location: location,
        templateId: templateId,
        tipId: tipId,
        done: false, // 消息发送状态设置为 false
      },
    });
    return result;
  } catch (err) {
    console.log(err);
    return err;
  }
};
