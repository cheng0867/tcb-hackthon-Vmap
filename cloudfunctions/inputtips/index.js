const request = require('request');
const urlencode = require('urlencode');

exports.main = async (event, context) => {
  const { latitude, longitude, city, keyword } = event
  return new Promise(resolve => {
    inputtips(latitude, longitude, city, keyword, resolve);
  })
}

async function inputtips(latitude, longitude, city, keyword, callback) {
  keyword = urlencode(keyword);
  const url = `https://restapi.amap.com/v3/assistant/inputtips?key=72932302ec093dc68265763da36ecf41&keywords=${keyword}&location=${longitude},${latitude}&city=${city}&datatype=all`;
  request(url, function (error, response, body) {
    let code = response.statusCode.toString(), result = null
    if (code == '200') {
      let res = JSON.parse(body);
      if (res.status == "1" && res.info == "OK") {
        result = res;
      } else {
        console.error(res)
      }
    }
    callback && callback(result);
  });
}