const cloud = require('wx-server-sdk');
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
});

exports.main = async (event, context) => {
  // cloud.init();
  const db = cloud.database();
  const _ = db.command;

  try {
    /*     // 从云开数据库中查询等待发送的消息列表
        const messages = await db
          .collection('messages')
          // 查询条件这里做了简化，只查找了状态为未发送的消息
          // 在真正的生产环境，可以根据开课日期等条件筛选应该发送哪些消息
          .where({
            done: false,
          })
          .get();
        console.log("heheheheh");
        console.log(messages.data.length); */


    const messages = await db
      .collection('messages')
      // 查询条件这里做了简化，只查找了状态为未发送的消息
      // 在真正的生产环境，可以根据开课日期等条件筛选应该发送哪些消息
      .where({
        done: false,
      })
      .get();

    messages.data.forEach(async m => {
      const arr = m.location.split(",")
      const lng = arr[0]
      const lat = arr[1]
      const infected = await db.collection("infectedLocations").where({
        location: _.geoNear({
          geometry: db.Geo.Point(lng, lat),
          minDistance: 0,
          maxDistance: 2000,
        })
      }).get()

      if (infected.data.length > 0) {
        //发送消息
        await cloud.openapi.subscribeMessage.send({
          touser: m.touser,
          page: m.page,
          data: "您关注的小区附近有感染！",
          templateId: m.templateId,
        });

        db.collection('messages').doc(m._id).update({
          data: {
            done: true,
          },
        });
      }

    })



    // // 循环消息列表
    // // const sendPromises = messages.data.map(async message => {
    // try {
    //   // 发送订阅消息
    //   // await cloud.openapi.subscribeMessage.send({
    //   await cloud.openapi.templateMessage.send.send({
    //     // touser: message.touser,
    //     touser: cloud.getWXContext().OPENID,
    //     page: 'index',
    //     data: {
    //       thing5: { value: '龙岗区图书馆' },
    //       thing4: { value: '该地点出现感染，请安全后再去该地' },
    //       date2: { value: '2020-02-07' },
    //       phrase3: { value: '危险' },
    //     },
    //     // templateId: message.templateId,
    //     templateId: "33K0X0VfZbi0JP9bL-G8zzQRwQRHlI62Ak231ACzxMA",
    //   });
    //   // 发送成功后将消息的状态改为已发送
    //   return db
    //     .collection('messages')
    //     .doc(message._id)
    //     .update({
    //       data: {
    //         done: true,
    //       },
    //     });
    // }
    // catch (e) {
    //   return e;
    // }
    // // });

    // return Promise.all(sendPromises);
  } catch (err) {
    console.log(err);
    return err;
  }
};