// 云函数入口文件
const cloud = require('wx-server-sdk');
cloud.init();
const db = cloud.database();
// 云函数入口函数
exports.main = async (event, context) => {
  return new Promise(resolve => {
    const { OPENID } = cloud.getWXContext();
    db.collection('messages').where({
      _openid: OPENID
    }).get().then(res => {
      if (res.errMsg == "collection.get:ok") {
        resolve(res.data);
      } else {
        console.log(res);
        resolve(null);
      }
    }).catch(err => {
      console.log(err);
      resolve(null);
    });
  })
}